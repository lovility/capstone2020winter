# Capstone Project 2020 Winter - RESTful Web API - Dima3API
This is a project for INFO2300-20W-Sec4-Project Development

## How to build install and use project

You�ll start by installing VISUAL STUDIO 2019 to run project.

1. Click Dima3API.sln in the Dima3API folder.
2. Excute the project in Visual Studio 2019
3. Run(F5)Dima3API
4. Check JSON data from server(MS SQL Server)

## License & copyright

Copyright (c) Jay Jang, Conestoga College Computer Programmer / Analyst

Licensed under the [MIT License](LICENSE).

##Change something...
It changed:)